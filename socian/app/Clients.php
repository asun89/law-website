<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    public function clients_type()
    {
        return $this->belongsTo('App\ClientsType');
    }
}
