<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientsType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients_type';

    public function clients()
    {
    	return $this->hasMany('App\Clients');
    }
}
