<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\JobsDescription;
use App\JobsApplication;

use Illuminate\Http\Request;
use Validator;

use Mail;

use App\JobsApp;

class CareerController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three Home-four';
        $this->jobs = JobsDescription::where('language_code', '=', $request->session()->get('lang'))->get();

        $this->custom_js = "
    alertify.defaults = {
        autoReset:true,
        basic:false,
        closable:true,
        closableByDimmer:true,
        frameless:false,
        maintainFocus:true, 
        maximizable:true,
        modal:true,
        movable:true,
        moveBounded:false,
        overflow:true,
        padding: true,
        pinnable:true,
        pinned:true,
        preventBodyShift:false, 
        resizable:true,
        startMaximized:false,
        transition:'fade',
        notifier:{
            delay:5,
            position:'bottom-right',
            closeButton: false
        },
        glossary:{
            title:'Ariyanto Arnaldo Law Firm',
            ok: 'OK',
            cancel: 'Cancel'            
        },
        theme:{
            input:'ajs-input',
            ok:'ajs-ok',
            cancel:'ajs-cancel'
        }
    };
        ";

        if($request->input('message') != "")
        {
            if($request->input('message') == "success")
            {
                $this->custom_js .= '
                alertify.alert("'.trans('messages.career_alert').'");
                ';
            }
            else
            {
                $this->custom_js .= '
                alertify.alert("Error!!! '.urldecode($request->input('message')).'");
                ';
            }
        }
        

        $this->js_files = array(
        );

        return view('career', array(
            'themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js, 
            'jobs' => $this->jobs, 
        ));

    }

    public function submit(Request $request)
    {
        // echo 'Career Page not ready yet';
        $post = $request->input();

        //Processing Uploaded File
        if (
            $request->hasFile('cv') && $request->file('cv')->isValid()
            && $request->hasFile('photo') && $request->file('photo')->isValid()
            ) 
        {

            $file = $request->file('cv');
            $file = $request->cv;

            $photo = $request->file('photo');
            $photo = $request->photo;

            $valid_files = array('pdf', 'doc', 'docx'); 
            $valid_files_photo = array('png', 'jpg', 'jpeg');

            if(
                in_array(strtolower($request->cv->extension()), $valid_files)
                && in_array(strtolower($request->photo->extension()), $valid_files_photo)
            )
            {

                $validator = Validator::make($request->all(), [
                    // 'photo' => 'dimensions:width=354,height=472'
                ]);

                if ($validator->fails()) {
                    return redirect('career?message=File width and height should be 354 x 472');
                }

                //Send Email and Insert to Database
                if(
                    isset($post['name']) && $post['name'] != "" &&
                    isset($post['email']) && $post['email'] != "" &&
                    isset($post['telephone']) && $post['telephone'] != "" &&
                    isset($post['facebook_url']) && $post['facebook_url'] != "" &&
                    isset($post['instagram_url']) && $post['instagram_url'] != "" &&
                    isset($post['position']) && $post['position'] != ""
                )
                {
                    try{
                    //Store File to be uploaded
                    $filename = 'cv_nama_'.strtolower(str_replace(" ","_",strip_tags($post['name']))).time().'.'.$request->cv->getClientOriginalExtension();
                    $path = $request->cv->move(public_path('cv'), $filename);

                    $photo_filename = 'photo_nama_'.strtolower(str_replace(" ","_",strip_tags($post['name']))).time().'.'.$request->photo->getClientOriginalExtension();
                    $photo_path = $request->photo->move(public_path('profile_pic'), $photo_filename);

                    $jobs_application = new JobsApplication;
                    $jobs_application->jobs_application_fullname = strip_tags($post['name']);
                    $jobs_application->jobs_id = strip_tags($post['position']);
                    $jobs_application->jobs_application_email = strip_tags($post['email']);
                    $jobs_application->jobs_application_phone = strip_tags($post['telephone']);
                    $jobs_application->jobs_application_facebook_url = strip_tags($post['facebook_url']);
                    $jobs_application->jobs_application_instagram_url = strip_tags($post['instagram_url']);
                    $jobs_application->jobs_application_cv = $filename;
                    $jobs_application->jobs_application_photo = $photo_filename;
                    $jobs_application->created_at = date('Y-m-d H:i:s');
                    $jobs_application->updated_at = date('Y-m-d H:i:s');
                    $jobs_application->deleted_at = NULL;
                    $jobs_application->save();

                    $jobs_app = new JobsApp();
                    $jobs_app->applicant_name = strip_tags($post['name']);
                    $jobs_app->applicant_position = JobsDescription::where('jobs_id', '=', strip_tags($post['position']))->first()->jobs_title;
                    $jobs_app->applicant_email = strip_tags($post['email']);
                    $jobs_app->applicant_phone = strip_tags($post['telephone']);
                    $jobs_app->applicant_facebook = strip_tags($post['facebook_url']);
                    $jobs_app->applicant_instagram = strip_tags($post['instagram_url']);
                    $jobs_app->applicant_cv = $filename;
                    $jobs_app->applicant_photo = $photo_filename;

                    // Mail::to(array(strip_tags($post['email'])))->send(new CareerApplyJob($jobs_app));

                    $data = array(
                        'jobs_application' => $jobs_app
                    );

                    Mail::send('emails.career_apply_job', $data, function ($m) use ($jobs_app) {
                        $m->from('info@ariyantoarnaldo.com', 'No-Reply Ariyantoarnaldo.com');
                        $m->to("cybercreation89@gmail.com", $jobs_app->applicant_name)->subject("Job Application at Ariyanto & Arnaldo Law Firm")->attach(public_path('cv/'.$jobs_app->applicant_cv))->attach(public_path('profile_pic/'.$jobs_app->applicant_photo));
                    });

                    // Mail::send('emails.career_apply_job', $data, function ($m) use ($jobs_app) {
                    //     $m->from('info@ariyantoarnaldo.com', 'No-Reply Ariyantoarnaldo.com');
                    //     $m->to("info@ariyantoarnaldo.com", 'Career @ Ariyantoarnaldo.com')->subject("Job Application at Ariyanto & Arnaldo Law Firm")->attach(public_path('cv/'.$jobs_app->applicant_cv))->attach(public_path('profile_pic/'.$jobs_app->applicant_photo));
                    // });

                        unlink(public_path('cv/'.$jobs_app->applicant_cv));
                        unlink(public_path('profile_pic/'.$jobs_app->applicant_photo));
                        return redirect('career?message=success');
                    } catch (\Exception $a){
                        print_r($a);
                        unlink(public_path('cv/'.$jobs_app->applicant_cv));
                        unlink(public_path('profile_pic/'.$jobs_app->applicant_photo));
                    }
                    
                    
                }
                else
                {
                    return redirect('career?message=Some fields are invalid');
                }
            }
            else
            {
                return redirect('career?message=Photo or CV is not Valid');
            }
        }
        else
        {
            return redirect('career?message=Photo and CV are required');
        }
    }
}