<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\ClientsType;

use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';
        $clients = ClientsType::with('clients')->get()->sortBy(function($clients)
		{
		    return $clients->clients->count();
		}, SORT_REGULAR, true);

        $max_count = 0;
		foreach($clients as $cl)
		{
			$max_count = count($cl->clients);
			break;
		}

        return view('clients', array(
        	'themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js, 
        	'max_count' => $max_count, 
        	'clients' => $clients, 
        	'language_code' => $request->session()->get('lang'))
        );
    }
}