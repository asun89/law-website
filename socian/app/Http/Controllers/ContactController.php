<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\ContactSubmission;
use App\ContactInquiry;
use App\Mail\ContactSendInquiry;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';

        $this->custom_js = "
    alertify.defaults = {
        autoReset:true,
        basic:false,
        closable:true,
        closableByDimmer:true,
        frameless:false,
        maintainFocus:true, 
        maximizable:true,
        modal:true,
        movable:true,
        moveBounded:false,
        overflow:true,
        padding: true,
        pinnable:true,
        pinned:true,
        preventBodyShift:false, 
        resizable:true,
        startMaximized:false,
        transition:'fade',
        notifier:{
            delay:5,
            position:'bottom-right',
            closeButton: false
        },
        glossary:{
            title:'Ariyanto Arnaldo Law Firm',
            ok: 'OK',
            cancel: 'Cancel'            
        },
        theme:{
            input:'ajs-input',
            ok:'ajs-ok',
            cancel:'ajs-cancel'
        }
    };
        ";

        if($request->input('message') == "success")
        {
            $this->custom_js .= '
            alertify.alert("'.trans('messages.contact_alert').'");
            ';
        }

        return view('contact', array(
        	'themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js, 
        	)

        );
    }

    public function submit(Request $request)
    {
        $post = $request->input();
        //Send Email and Insert to Database

        $validator = Validator::make($request->all(), [
           'name' => ['required','max:255','regex:/^[A-Za-z ]+$/'],
           'email' => ['required','max:255', 'regex:/^[A-Za-z0-9_.@ ]+$/'],
           'telephone' => ['required', 'max:20', 'regex:/^[0-9]+$/'],
           'message' => ['required', 'regex:/^[A-Za-z0-9_.,()\- ]+$/'],
       ]);

        if ($validator->fails()) {
            return redirect('contact?message=Some fields are invalid');
        }
        else {

            $contact_submission = new ContactSubmission;
            $contact_submission->contact_submission_name = strip_tags($post['name']);
            $contact_submission->contact_submission_email = strip_tags($post['email']);
            $contact_submission->contact_submission_phone = strip_tags($post['telephone']);
            $contact_submission->contact_submission_message = strip_tags($post['message']);
            $contact_submission->created_at = date('Y-m-d H:i:s');
            $contact_submission->updated_at = date('Y-m-d H:i:s');
            $contact_submission->deleted_at = NULL;
            $contact_submission->save();

            $contact_inquiry = new ContactInquiry();
            $contact_inquiry->contact_name = strip_tags($post['name']);
            $contact_inquiry->contact_email = strip_tags($post['email']);
            $contact_inquiry->contact_phone = strip_tags($post['telephone']);
            $contact_inquiry->contact_message = strip_tags($post['message']);

            $data = array(
                'contact_inquiry' => $contact_inquiry
            );

            Mail::send('emails.contact_send_inquiry', $data, function ($m) use ($contact_inquiry) {
                $m->from('info@ariyantoarnaldo.com', 'No-Reply Ariyantoarnaldo.com');
                $m->to($contact_inquiry->contact_email, $contact_inquiry->contact_name)->subject("Inquiry Submission at Ariyanto & Arnaldo Law Firm");
            });
        }

        return redirect('contact?message=success');
    }
}