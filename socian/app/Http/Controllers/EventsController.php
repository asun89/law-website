<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\Events;

use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three blog';

        $events = Events::where(
        	'language_code', '=', $request->session()->get('lang')
        )->orderBy('id', 'DESC')->paginate(6);

        return view('events', array('themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js, 'events' => $events));
    }

    public function detail($id, Request $request)
    {
        $this->themes = 'Home-three';

        $events_detail = Events::where(
            array(
                array('events_slug', '=', $id),
                array('language_code', '=', $request->session()->get('lang'))
            )
        )->get();

        $events = Events::where('language_code', '=', $request->session()->get('lang'))->get();
        return view('events_detail', array(
            'themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js, 
            'events_detail' => $events_detail[0],
            'events' => $events
        ));
    }
}