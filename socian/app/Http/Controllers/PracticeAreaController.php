<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\PracticeArea;

use Illuminate\Http\Request;

class PracticeAreaController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';
        $practice_area = PracticeArea::where('language_code', '=', $request->session()->get('lang'))->get();

        return view('practice_area', array(
            'themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js, 
            'practice_area' => $practice_area
        ));
    }

    public function detail($id, Request $request)
    {
    	$this->themes = 'Home-three';

        $practice_area = PracticeArea::where('id', '=', $id)->get();
        $practice_areas = PracticeArea::where('language_code', '=', $request->session()->get('lang'))->get();
        return view('practice_area_detail', array(
            'themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js, 
            'practice_area' => $practice_area[0],
            'practice_areas' => $practice_areas
        ));
    }
}