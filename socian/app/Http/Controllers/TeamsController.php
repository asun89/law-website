<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\Teams;

use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';
        $teams = Teams::where('language_code', '=', $request->session()->get('lang'))->orderBy('teams_precedence', 'ASC')->get();
        
        return view('teams', array('themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js,
            "teams" => $teams));
    }

    public function detail($id)
    {
        $this->themes = 'Home-three';
        $teams = Teams::where('id', '=', $id)->get();

        return view('teams_detail', array('themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js,
            "teams" => $teams[0]));
    }
}