<?php

namespace App\Http\Middleware;

use Closure;

class LocalizationHelper
{
    public function handle($request, Closure $next)
    {
        if($request->input('lang'))
        {
            $lang = $request->input('lang');
        }
        else
        {
            if($request->session()->get('lang'))
            {
                $lang = $request->session()->get('lang');      
            }
            else
            {
                $lang = 'en';
            }
        }

        $request->session()->put('lang', $lang); 
        
        \App::setLocale($lang);

        return $next($request);
    }
}
