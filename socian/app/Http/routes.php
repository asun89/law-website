<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@Index');
Route::get('/home', 'HomeController@Index');
Route::get('/about', 'AboutController@Index');
Route::get('/partners', 'PartnersController@Index');
Route::get('/partners/detail/{id}', 'PartnersController@Detail');
Route::get('/teams', 'TeamsController@Index');
Route::get('/teams/detail/{id}', 'TeamsController@Detail');
Route::get('/practice_area', 'PracticeAreaController@Index');
Route::get('/practice_area/detail/{id}', 'PracticeAreaController@Detail');
Route::get('/our_clients', 'ClientsController@Index');
Route::get('/events', 'EventsController@Index');
Route::get('/events/detail/{id}', 'EventsController@Detail');
Route::get('/career', 'CareerController@Index');
Route::post('/career/submit', 'CareerController@Submit');
Route::get('/contact', 'ContactController@Index');
Route::post('/contact/submit', 'ContactController@Submit');