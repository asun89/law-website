<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs_db';

    public function jobs_description()
    {
        return $this->hasMany('App\JobsDescription');
    }
}
