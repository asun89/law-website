<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobsApplication extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs_application';

    public function jobs()
    {
        return $this->belongsTo('App\JobsDescription');
    }
}
