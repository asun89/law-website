<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PracticeArea extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'practice_area';
}
