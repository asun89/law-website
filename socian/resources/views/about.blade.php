@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area" style="background-image:url({{URL::asset('assets/img/partners.jpg')}}) !important">
 <div class="container">
	<div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="main-bennar">
		  <h2>{{trans('messages.about_us')}}</h2>
		  <div class="breadcumb">
			<ul>
			  <li> <a href="{{URL::to('home')}}">Home</a> </li>
			  <li>{{trans('messages.about_us')}}</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!--  Banner Area End here -->
<!-- About Page Lawyer Section Area Start here -->
<div class="about-page-section">
 <div class="container">
	 <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  <div class="about-page-content">
			<h2>{{trans('messages.about_title')}}</h2>
			<br/>
			<p>
				 {!!$about_content['main_content']!!}
			</p>

			<div class="contact-us-button">
			  <a href="contact">{{trans('messages.contact_us')}}</a>
			</div>
		  </div>
		</div>
		<!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  <div class="about-page-featured-image">
			<img src="img/about-feature.png" alt="">
		  </div>
		</div>-->
	  </div>

	  <!-- Section 2 of About is -->
	  <!--<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  <div class="about-page-featured-image">
			<img src="img/about-feature.png" alt="">
		  </div>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		  <div class="about-page-content">
			<h2>About Our <span> History</span></h2>
			<p class="text-blod">Dhere are many variations of passages of Lorem Ipsum availabbut the humourrandomisedwords.There are many variations of passages of Lorem Ipsum availablebut the majority.</p>
			<p>Derdiam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Nam liber tempor cum soluta nobis eleifend.</p>

			<div class="contact-us-button">
			  <a href="contact">Contact Us</a>
			</div>
		  </div>
		</div>
	  </div>-->
  </div>
</div>
<!-- About Page Lawyer Section Area End here -->

  <!-- Experience Area Start here -->
<div class="experince-area">
	<div class="container">
	  <div class="row">
		 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="single-experince-area">
			  <div class="icon">
				<a href="#"><i class="fa fa-bookmark-o"></i></a>
			  </div>
			  <h3><a href="#">Certification</a></h3>
			  <p>Business many variations of passages of Lorem Ipsum availablesuffhuof passages of Lorem Ipsum available, but the majority h</p>
			</div>
		 </div>
		 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="single-experince-area">
			  <div class="icon">
				<a href="#"><i class="fa fa-pencil-square-o"></i></a>
			  </div>
			  <h3><a href="#">Legal Help</a></h3>
			  <p>Business many variations of passages of Lorem Ipsum availablesuffhuof passages of Lorem Ipsum available, but the majority h</p>
			</div>
		 </div>
		 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="single-experince-area">
			  <div class="icon">
				<a href="#"><i class="fa fa-clock-o"></i></a>
			  </div>
			  <h3><a href="#">27/7 Opened</a></h3>
			  <p>Business many variations of passages of Lorem Ipsum availablesuffhuof passages of Lorem Ipsum available, but the majority h</p>
			</div>
		 </div>
	  </div>
	</div>
  </div>
<!-- Experience Area End here -->
<!-- Make An Appointment Area Start here -->
<div class="make-appointment-area">
  <div class="container">
	 <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  <div class="make-appointment">
			<h2>Need An Appointment For Legal HElp?</h2>
			<a href="#">Make An Appointment</a>
		  </div>
		 </div>
	  </div>
  </div>
</div>
<!-- Make An Appointment Area End here -->
<!-- Our Attorney Start Here -->
<!--<div class="our-attorney-area">
<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>Our Partners</h2>
		 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapien3e sint odit iusto blanditiis doloribus.</3>
	   </div>
	</div>
  </div>
  <div class="our-attorney">
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/3.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">Jhon Doe</a></h3>
		<p class="designation">Lawyer & Founder</p>
	  </div>
	</div>
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/1.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">M.Albert</a></h3>
		<p class="designation">Lawyer & Founder</p>
	  </div>
	</div>
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/2.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">Jhon Doe</a></h3>
		<p class="designation">Lawyer & Founder</p>
	  </div>
	</div>
	
  </div>
</div>
</div>-->
<!-- Our Attorney End Here -->
@endsection