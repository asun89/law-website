@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area" style="background-image:url({{URL::asset('assets/img/career.jpg')}}) !important">
 <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="main-bennar">
          <h2>{{trans('messages.career')}}</h2>
          <div class="breadcumb">
            <ul>
              <li> <a href="{{URL::to('home')}}">{{trans('messages.home')}}</a> </li>
              <li>{{trans('messages.career')}}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  Banner Area End here -->

<!-- Lawer Expert Area Start here -->
<div class="lawyer-expert-area">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="section-title-area">
         <h2>{{trans('messages.lets_join_us')}}</h2>
         <p>{!!nl2br(e(trans('messages.career_message')))!!}
         </p>
       </div>
     </div>              
    </div>
</div>
<!-- Lawer Expert Area End here -->

<!-- Home Four Practices Area Start here -->
<div class="home4-practiceing-area">
 <div class="container">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="home4-practiceing-content-area">
        <div>
        <ul class="nav nav-tabs practice-tab" role="tablist">

        <?php $count = 0; ?>
        @foreach($jobs as $j)
        <?php $count++; ?>
        <li role="presentation" @if($count == 1) class="active" @endif><a href="#job{{$j->jobs_id}}" data-controls="home" role="tab" data-toggle="tab">{{$j->jobs_title}}</a></li>
        @endforeach
        </ul>
        <div class="tab-content practice-content">

        <?php $count = 0; ?>
        @foreach($jobs as $j)
        <?php $count++; ?>
        <div role="tabpanel" class="tab-pane @if($count == 1) active @endif" id="job{{$j->jobs_id}}">
          <h2>{{$j->jobs_title}}</h2>
          
          <h3>Responsibilities : </h3>
          <p>{!!nl2br(e($j->jobs_description))!!}</p>

          <h3>Requirements : </h3>
          <p>{!!nl2br(e($j->jobs_requirement))!!}</p>
        </div>
        @endforeach
        
        </div>
        </div>
       </div>
      </div>
     
 </div>
</div>
<!-- Home Four Practices Area End here -->

<!-- Main Contact page Start here -->

  <div class="contact-page-area">
  <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="contact-form-area">
      <h2>{{trans('messages.quick_apply_title')}}</h2>
      
        <form enctype="multipart/form-data" action="{{URL::to('career/submit')}}" method="POST">
        <fieldset>
          {{ csrf_field() }}
          <div class="col-sm-12">
          <div class="form-group">
          <p>{!!nl2br(e(trans('messages.quick_apply_description')))!!}</p>
          <p></p>
          </div>
          <div class="form-group">
            <label>{{trans('messages.candidate_full_name')}} : </label>
            <input type="text" name="name" class="form-control" required>
          </div>
          </div>
          <div class="col-sm-12">
          <div class="form-group">
            <label>{{trans('messages.candidate_email')}} : </label>
            <input type="email" name="email" class="form-control" required>
          </div>
          </div>
          <div class="col-sm-12">
          <div class="form-group">
            <label>{{trans('messages.candidate_phone')}} : </label>
            <input type="number" name="telephone" class="form-control" required>
          </div>
          </div>
          <div class="col-sm-12">
          <div class="form-group">
            <label>{{trans('messages.candidate_facebook')}} : </label>
            <input type="text" name="facebook_url" class="form-control" required>
          </div>
          </div>
          <div class="col-sm-12">
          <div class="form-group">
            <label>{{trans('messages.candidate_instagram')}} : </label>
            <input type="text" name="instagram_url" class="form-control" required>
          </div>
          </div>
          <div class="col-sm-12">
          <div class="form-group">
            <label>{{trans('messages.candidate_position')}} : </label>
            <select name="position" class="form-control">
              <option value="">-Select Position-</option>
              @foreach($jobs as $j)
                <option value="{{$j->jobs->id}}">{{$j->jobs_title}}</option>
              @endforeach
            </select>
          </div>
          </div>
          <div class="col-sm-12">
          <div class="form-group">
            <label>{{trans('messages.candidate_cv')}} <br/><span style="font-size:10px;">*Only PDF, DOC, DOCX Allowed</span>: </label>
            <input type="file" name="cv" class="textarea form-control"/>
          </div>
          </div>

          <div class="col-sm-12">
          <div class="form-group">
            <label>{{trans('messages.candidate_photo')}} <br/><span style="font-size:10px;">*Only PNG or JPEG Allowed</span>: </label>
            <input type="file" name="photo" class="textarea form-control"/>
          </div>
          </div>


          <div class="col-sm-12">
          <div class="form-group">
            <button class="btn-send submit-buttom" type="submit">{{trans('messages.submit_cv')}}</button>
          </div>
          </div>
        </fieldset>
      </form>

      </div>
    </div>
    </div>
  </div>
  </div>
  <!-- Main Contact page End here -->
@endsection
