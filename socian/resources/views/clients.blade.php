@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area" style="background-image:url({{URL::asset('assets/img/clients.jpg')}}) !important">
 <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="main-bennar">
          <h2>{{trans('messages.our_clients')}}</h2>
          <div class="breadcumb">
            <ul>
              <li> <a href="{{URL::to('home')}}">{{trans('messages.home')}}</a> </li>
              <li>{{trans('messages.our_clients')}}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  Banner Area End here -->
<!-- Happy Client Area Start Here -->
<div class="happy-client-area">
<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>{{trans('messages.clients_title')}}</h2>
		 <p><!--{!!trans('messages.clients_description')!!}--></p>
	   </div>
	</div>
  </div>
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <?php
    $count = 0;
    $max_height = 100;
		foreach($clients as $cl)
    {
      if($count == 2 || $count == 0)
      {
        $count = 0;
        $max_count = count($cl->clients);
        $max_height = (30 * $max_count);
      }
      if($max_height < 100)
      {
        $max_height = 100;
      }
      $count++;
    ?>

    @if(count($cl->clients) > 0)
	   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 clients_section" style="height:{{ $max_height }}px; overflow: hidden;">
	   		@if($language_code == 'en')
	   	  <h5>{{$cl->clients_type_name_en}}</h5>
	   	  @else
	   	  <h5>{{$cl->clients_type_name_id}}</h5>
	   	  @endif
          <ul>
          	@foreach($cl->clients as $c)
            <li>{{$c->clients_name}}</li>
            @endforeach
          </ul>    
      </div>
      @endif
    <?php
    } 
    ?>

	</div>
  </div>
</div>
</div>
<!-- Happy Client Area End Here -->
@endsection