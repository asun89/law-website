@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
   <div class="banner-area" style="background-image:url({{URL::asset('assets/img/contact.jpg')}}) !important">
	 <div class="container">
		<div class="row">
		  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="main-bennar">
			  <h2>{{trans('messages.contact_us')}}</h2>
			  <div class="breadcumb">
				<ul>
				  <li> <a href="{{URL::to('home')}}">{{trans('messages.home')}}</a> </li>
				  <li>{{trans('messages.contact_us')}}</li>
				</ul>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
   </div>
   <!--  Banner Area End here -->
  <!-- Main Contact page Start here -->
  <div class="contact-page-area">
	<div class="container">
	  <div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		  <div class="contact-form-area">
			<h2>{{trans('messages.contact_form')}}</h2>
			  <form action="{{URL::to('contact/submit')}}" method="POST">
			  {{ csrf_field() }}
				<fieldset>
				  <div class="col-sm-12">
					<div class="form-group">
					  <label>{{trans('messages.contact_name')}} : </label>
					  <input type="text" name="name" placeholder="" class="form-control" required>
					</div>
				  </div>
				  <div class="col-sm-12">
					<div class="form-group">
					  <label>{{trans('messages.contact_email')}} : </label>
					  <input type="email" name="email" class="form-control" required>
					</div>
				  </div>
				  <div class="col-sm-12">
					<div class="form-group">
					  <label>{{trans('messages.contact_phone')}} : </label>
					  <input type="number" name="telephone" class="form-control" required>
					</div>
				  </div>
				  <div class="col-sm-12">
					<div class="form-group">
					  <label>{{trans('messages.contact_message')}} : </label>
					  <textarea cols="40" rows="5" name="message" class="textarea form-control"></textarea>
					</div>
				  </div>
				  <div class="col-sm-12">
					<div class="form-group">
					  <button class="btn-send submit-buttom" type="submit">{{trans('messages.send_message')}}</button>
					</div>
				  </div>
				</fieldset>
			</form>
		  </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  <div class="office-address">
			<h2>{{trans('messages.office_address')}}</h2>
			<ul>
			  <li><a href="#"><i class="fa fa-map-marker"></i><span>
			  <b>ARIYANTOARNALDO LAW FIRM</b><br/>
Equity Tower, 35th Floor, Suite 35C</span></a></li>
			  <li><a href="#"><i class="fa fa-phone"></i><span>{{trans('messages.working_phone')}}</span></a></li>
			  <li><a href="#"><i class="fa fa-envelope-o"></i><span>{{trans('messages.working_email')}}</span></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <!-- Main Contact page End here -->
  <div class="google-map-area">
	<iframe width="100%" height="370" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJzVhQglHxaS4RokuFQ3HxvEY&key=AIzaSyC_zzRJtC4yr4lz-Qb8k3ifGP4RzSScm48" allowfullscreen></iframe>
  </div>
	
@endsection