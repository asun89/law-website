@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area" style="background-image:url({{URL::asset('assets/img/events.jpg')}}) !important">
 <div class="container">
	<div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="main-bennar">
		  <h2>{{trans('messages.events')}}</h2>
		  <div class="breadcumb">
			<ul>
			  <li> <a href="{{URL::to('home')}}">{{trans('messages.home')}}</a> </li>
			  <li>{{trans('messages.events')}}</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!--  Banner Area End here --><!-- Latest News Area Start Here -->
<div class="latest-news-area">
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="section-title-area">
             <h2>{{trans('messages.latest_events')}}</h2>
             <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapiente sint odit iusto blanditiis doloribus.</p>-->
           </div>
        </div>            
    </div>
    <div class="row">
        @foreach($events as $ev)
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
           <div class="single-news-area">
             <div class="news-featured-image">
               <a href="{{URL::to('events/detail/'.$ev->events_slug)}}"><img src="{{URL::asset('assets/img/events/'.$ev->events_thumbnail)}}" alt="image"></a>              
               <ul>
                 <li class="active">{{date('M,d Y', strtotime($ev->events_date))}}</li>
                 <li><!--<a href="#" onclick="return false"><i class="fa fa-user"> </i> by admin--></a></li>
                 <li><a href="#" onclick="return false"><i class="fa fa-eye"></i> 02 </a></li>
               </ul>
             </div>
             <h3><a href="{{URL::to('events/detail/'.$ev->events_slug)}}">{{$ev->events_title}}</a></h3>
             <p>{{$ev->events_short_content}}</p>
           </div>
        </div>
        @endforeach                  
    </div>
    <div class="pagination-area">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                  
          <div class="pagination">
              <!--<ul class="text-center">
                <li class="active"><a href="">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li><a href="">5</a></li>
              </ul>-->

              {!!str_replace('class="pagination"', 'class="text-center"', $events ->links())!!}
          </div>
        </div>
      </div>
    </div>
</div>
</div>
<!-- Latest News Area End Here -->
@endsection