@extends('templates.layout')
@section('title', 'Page Title')

@section('content')<!--  Banner Area Start here -->
       <div class="banner-area" style="background-image:url({{URL::asset('assets/img/practice_area.jpg')}}) !important">
         <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="main-bennar">
                  <h2>{{$events_detail->events_title}}</h2>
                </div>
              </div>
            </div>
          </div>
       </div>
       <!--  Banner Area End here -->
    <!-- Blog Area Start -->
    <div class="total-blog-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="single-blog-post">
              <div class="blog-image">
                <a href="{{URL::to('practice_area/detail/'.$events_detail->id)}}"><img src="{{ URL::asset('img/events/'.$events_detail->events_image)}}" alt="family"></a>
              </div>  
              <h2>{{$events_detail->events_title}}</h2>
              <p class="text-bold">{!!nl2br(e($events_detail->events_long_content))!!}</p>
            </div>
          
          </div>

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="blog-sidebar-area">
              <div class="single-sidebar">
                <h5>Other Events : </h5>
                <div class="sidebar-category">
                  <ul>
                    @foreach ($events as $ev)
                    <li><a href="{{URL::to('practice_area/detail/'.$ev->id)}}">{{$ev->events_title}}</a></li>
                    @endforeach
                  </ul>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Blog Area End -->
@endsection