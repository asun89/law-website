@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!-- slider -->
<div class="slider-area">
	<div class="bend niceties preview-2">
		<div id="ensign-nivoslider" class="slides"> 
			@foreach ($home_slider as $slider)
			<img src="{{URL::asset('assets/img/slides/'.$slider->slider_image)}}" alt="image" title="#slider-direction-{{$slider->id}}"  />
			@endforeach
		</div>
		@foreach ($home_slider as $slider)
		<!-- direction 1 -->
		<div id="slider-direction-{{$slider->id}}" class="t-cn slider-direction">
			<div class="slider-progress"></div>
			<div class="slider-content t-cn s-tb slider-{{$slider->id}}">
				<div class="title-container s-tb-c title-compress">
					<div data-wow-delay="0.2s" data-wow-duration="2s" class="tp-caption big-title rs-title customin customout bg-sld-cp-primary wow zoomIn" style="visibility: visible; animation-duration: 2s; animation-delay: 0.2s; animation-name: zoomIn;">
					{!!html_entity_decode($slider->slider_title)!!}
					</div>
					<!--<div data-wow-delay="0.3s" data-wow-duration="3s" class="tp-caption small-content customin customout rs-title-small bg-sld-cp-primary tp-resizeme rs-parallaxlevel-0 wow zoomIn" style="visibility: visible; animation-duration: 3s; animation-delay: 0.3s; animation-name: zoomIn;">
					{{$slider->slider_tagline}}
					</div>-->
				</div>
				<div data-wow-delay="0.4s" data-wow-duration="4s" class="button wow fadeIn" style="visibility: visible; animation-duration: 4s; animation-delay: 0.4s; animation-name: fadeIn;">
				<a href="{{URL::to($slider->slider_button_link)}}" class="btn btn-success">{{$slider->slider_button_text}}</a>
				</div>
			</div>  
		</div>
		@endforeach

		<!-- direction 2 -->
		<!--<div id="slider-direction-2" class="slider-direction">
		  <div class="slider-progress"></div>
		  <div class="slider-content t-lfr s-tb slider-2">
			<div class="title-container s-tb-c">
			  <h1 class="title1">To Get Our Proper Justice</h1>                     
			  <div data-wow-delay="0.3s" data-wow-duration="3s" class="tp-caption small-content customin customout rs-title-small bg-sld-cp-primary tp-resizeme rs-parallaxlevel-0 wow zoomIn" style="visibility: visible; animation-duration: 3s; animation-delay: 0.3s; animation-name: zoomIn;">
			  Lowyer / Law Firm / Attorney
			  </div>
			  <div style="visibility: visible; animation-duration: 4s; animation-delay: 0.4s;" class="button wow fadeIn" data-wow-duration="4s" data-wow-delay="0.4s">
				<a class="btn btn-success" href="{{URL::to('contact')}}">Contact Us</a>
				</div>                     
			</div>
		  </div>  
		</div>-->

	</div>
</div>
<!-- slider end-->
<!-- Experience Area Start here -->
<div class="experince-area">
  <div class="container">
	<div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 <div class="experince">
		   <h2>{{trans('messages.welcome')}}</h2>
		   <p>{{trans('messages.home_message_1')}}
			<br/><br/>
			{{trans('messages.home_message_2')}}</p>
		 </div>
	   </div>
	</div>
	<!--
	<div class="row">
	   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  <div class="single-experince-area">
			<div class="icon">
			  <a href="#" onclick="return false"><i class="fa fa-bookmark-o"></i></a>
			</div>
			<h3><a href="#" onclick="return false">Certification</a></h3>
			<p>Business many variations of passages of Lorem Ipsum availablesuffhuof passages of Lorem Ipsum available, but the majority h</p>
		  </div>
	   </div>
	   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  <div class="single-experince-area">
			<div class="icon">
			  <a href="#" onclick="return false"><i class="fa fa-pencil-square-o"></i></a>
			</div>
			<h3><a href="#" onclick="return false">Legal Help</a></h3>
			<p>Business many variations of passages of Lorem Ipsum availablesuffhuof passages of Lorem Ipsum available, but the majority h</p>
		  </div>
	   </div>
	   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  <div class="single-experince-area">
			<div class="icon">
			  <a href="#" onclick="return false"><i class="fa fa-clock-o"></i></a>
			</div>
			<h3><a href="#" onclick="return false">24/7 Opened</a></h3>
			<p>Business many variations of passages of Lorem Ipsum availablesuffhuof passages of Lorem Ipsum available, but the majority h</p>
		  </div>
	   </div>
	</div>
	-->
  </div>
</div>
<!-- Experience Area End here -->
<!-- Make An Appointment Area Start here -->
<div class="make-appointment-area">
  <div class="container">
	 <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  <div class="make-appointment">
			<h2>{{trans('messages.need_legal_help')}}</h2>
			<a href="contact">{{trans('messages.need_legal_help_button_text')}}</a>
		  </div>
		 </div>
	  </div>
  </div>
</div>
<!-- Make An Appointment Area End here -->
<!-- Our Practice Area Start Here -->
<div class="our-practice-area">
 <div class="container">
   <div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>{{trans('messages.our_practice_area')}}</h2>
	   </div>
	 </div>
   </div>
 </div>
 <div class="practice-area">
   <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="overflow: hidden;">
		  <ul class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			  @foreach($practice_area1 as $practice)
			  <!--<div class="single-practice">
			   <div class="single-practice-content">
				 <i class="fa fa-{{$practice->practice_area_icon}}"></i>
				 <h3><a href="{{URL::to('practice_area/detail/'.$practice->id)}}">{{$practice->practice_area_name}}</a></h3>
				 <p>{{$practice->practice_area_short_description}}</p>
				 <div class="read-more">
				   <a href="{{URL::to('practice_area/detail/'.$practice->id)}}">Show More</a>
				 </div>
			  </div>        
			  </div> -->

			  
			  	<li><a href="{{URL::to('practice_area/detail/'.$practice->id)}}">{{$practice->practice_area_name}}</a></li>
			  @endforeach
		  </ul>
	  </div>

	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="overflow: hidden;">
		  <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  @foreach($practice_area2 as $practice)
			  <!--<div class="single-practice">
			   <div class="single-practice-content">
				 <i class="fa fa-{{$practice->practice_area_icon}}"></i>
				 <h3><a href="{{URL::to('practice_area/detail/'.$practice->id)}}">{{$practice->practice_area_name}}</a></h3>
				 <p>{{$practice->practice_area_short_description}}</p>
				 <div class="read-more">
				   <a href="{{URL::to('practice_area/detail/'.$practice->id)}}">Show More</a>
				 </div>
			  </div>        
			  </div> -->

			  
			  	<li><a href="{{URL::to('practice_area/detail/'.$practice->id)}}">{{$practice->practice_area_name}}</a></li>
			  @endforeach
		  </ul>
	  </div>
	  </div>
 </div>
</div>
</div>
<!-- Our Practice Area End Here -->
<!-- Advertise Banner Area Start -->
<!--<div class="advertise-area">
 <div class="container">
   <div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="bennar">
		  <h2>{!!nl2br(e(trans('messages.pay_nothing')))!!}</h2>
		</div>
	  </div>
   </div>
 </div>
</div>-->
<!-- Advertise Banner Area End -->
<!-- Happy Client Area Start Here -->
<!--<div class="happy-client-area">
<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>Happy Client</h2>
		 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapiente sint odit iusto blanditiis doloribus.</p>
	   </div>
	</div>
  </div>
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="client-section-area">
		<div class="client-section">
		  <div class="single-client-area">
		   <h3><a href="#">Jhon Albert</a></h3>
			 <p class="designation">CEO, Jhon</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/0.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Robert Albert</a></h3>
			<p class="designation">CEO, Robert</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/00.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Morey Albert</a></h3>
			<p class="designation">CEO, Morey</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/01.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Robert Albert</a></h3>
			<p class="designation">CEO, Robert</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/00.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Jhon Albert</a></h3>
			<p class="designation">CEO, Jhon</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/0.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Robert Albert</a></h3>
			<p class="designation">CEO, Robert</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/00.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Morey Albert</a></h3>
			<p class="designation">CEO, Morey</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/01.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Robert Albert</a></h3>
			<p class="designation">CEO, Robert</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/00.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Jhon Albert</a></h3>
			<p class="designation">CEO, Jhon</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/0.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		  <div class="single-client-area">
			<h3><a href="#">Robert Albert</a></h3>
			<p class="designation">CEO, Robert</p>
			<div class="picture">
			  <a href="#"><img src="img/clients/00.jpg" alt=""></a>
			  <ul>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li class="disable"><a href="#"><i class="fa fa-star"></i></a></li>
			  </ul>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque officiis alias sed officia blanditiis rerum, a harum, veritatis iste laborum illum distinctio consequuntur accusantium velit vitae molestiae. Inventore, minus, modi!</p>
		  </div>
		</div>
	   </div>
	</div>
  </div>
</div>
</div>-->
<!-- Happy Client Area End Here -->
<!-- Our Attorney Start Here -->
<!--
<div class="our-attorney-area">
<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>Our Attorney</h2>
		 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapiente sint odit iusto blanditiis doloribus.</p>
	   </div>
	</div>
  </div>
  <div class="our-attorney">
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/3.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">Jhon Doe</a></h3>
		<p>Lawyer & Founder</p>
	  </div>
	</div>
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/1.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">M.Albert</a></h3>
		<p>Lawyer & Founder</p>
	  </div>
	</div>
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/2.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">Jhon Doe</a></h3>
		<p>Lawyer & Founder</p>
	  </div>
	</div>
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/4.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">M.Albert</a></h3>
		<p>Lawyer & Founder</p>
	  </div>
	</div>
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/1.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">Jhon Doe</a></h3>
		<p>Lawyer & Founder</p>
	  </div>
	</div>
	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="#"><img src="img/attorney/2.jpg" alt=""></a>
		<div class="overlay">
		  <h2><a href="#">View Bio</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="#">Jhon Doe</a></h3>
		<p>Lawyer & Founder</p>
	  </div>
	</div>
  </div>
</div>
</div>
-->
<!-- Our Attorney End Here -->
<!-- Latest News Area Start Here -->
<div class="latest-news-area">
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		   <div class="section-title-area">
			 <h2>{{trans('messages.latest_events')}}</h2>
		   </div>
		</div>            
	</div>
	<div class="row">
		@foreach($events as $ev)
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
           <div class="single-news-area">
             <div class="news-featured-image">
               <a href="{{URL::to('events/detail/'.$ev->id)}}"><img src="{{URL::asset('assets/img/events/'.$ev->events_thumbnail)}}" alt="image"></a>              
               <ul>
                 <li class="active">{{date('M,d Y', strtotime($ev->events_date))}}</li>
                 <li><!--<a href="#" onclick="return false"><i class="fa fa-user"> </i> by admin--></a></li>
                 <li><a href="#" onclick="return false"><i class="fa fa-eye"></i> 02 </a></li>
               </ul>
             </div>
             <h3><a href="{{URL::to('events/detail/'.$ev->id)}}">{{$ev->events_title}}</a></h3>
             <p>{{$ev->events_short_content}}</p>
           </div>
        </div>
        @endforeach                   
	</div>
</div>
</div>
<!-- Latest News Area End Here -->
@endsection