@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area" style="background-image:url({{URL::asset('assets/img/partners.jpg')}}) !important">
 <div class="container">
	<div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="main-bennar">
		  <h2>{{trans('messages.partners')}}</h2>
		  <div class="breadcumb">
			<ul>
			  <li> <a href="{{URL::to('home')}}">{{trans('messages.home')}}</a> </li>
			  <li>{{trans('messages.partners')}}</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!--  Banner Area End here -->
<!-- Make An Appointment Area End here -->
<!-- Our Attorney Start Here -->
<div class="our-attorney-area">
<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>{{trans('messages.partners_title')}}</h2>
		 <!--<p>{{trans('messages.partners_caption')}}</3>-->
	   </div>
	</div>
  </div>
  <div class="our-attorney partners">
	
	@foreach($partners as $p)
	<!--<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="{{URL::to('partners/detail/'.$p->id)}}"><img src="{{URL::asset('assets/img/attorney/'.$p->partners_photo)}}" alt="{{$p->partners_name}}"></a>
		<div class="overlay">
		  <h2><a href="{{URL::to('partners/detail/'.$p->partners_id)}}">{{trans('messages.view_bio')}}</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="{{$p->partners_facebook}}"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="{{$p->partners_twitter}}"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="{{$p->partners_linkedin}}"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="{{$p->partners_skype}}"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="{{URL::to('partners/detail/'.$p->partners_id)}}">{{$p->partners_name}}</a></h3>
		<p class="designation">{{$p->partners_position}}</p>
	  </div>
	</div>-->

	<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="{{URL::to('partners/detail/'.$p->partners_slug)}}"><img src="{{URL::asset('assets/img/attorney/'.$p->partners_photo)}}" alt="{{$p->partners_name}}"></a>
		<a href="{{URL::to('partners/detail/'.$p->partners_slug)}}"><div class="overlay">
		  <h2 class="partners-name">{{$p->partners_name}}</h2>
		  <div class="borderline"></div>
		  <h3 class="partners-position">{{$p->partners_position}}</h3>
		  <!--<div class="social-media">
			<ul>
			  <li><a href="{{$p->partners_facebook}}"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="{{$p->partners_twitter}}"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="{{$p->partners_linkedin}}"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="{{$p->partners_skype}}"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>-->
		</div></a>
	  </div>
	  <!--<div class="attorney-content">                
		<h3><a href="{{URL::to('partners/detail/'.$p->partners_id)}}">{{$p->partners_name}}</a></h3>
		<p class="designation">{{$p->partners_position}}</p>
	  </div>-->
	</div>
	@endforeach

  </div>
</div>
</div>
<!-- Our Attorney End Here -->
@endsection