@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area" style="background-image:url({{URL::asset('assets/img/partners_2.jpg')}}) !important">
 <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="main-bennar">
          <h2>{{trans('messages.partners_detail_title')}}</h2>
          <div class="breadcumb">
            <ul>
              <li> <a href="{{URL::to('home')}}">Home</a> </li>
              <li>{{trans('messages.partners_detail_breadcrumb')}}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  Banner Area End here -->
<!-- Team details Inner Area Start Here -->
<div class="team-details-inner-area">
<div class="container">
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="team-details-img-wraper">
        <div class="team-details-img-holder" style="margin-bottom:50px;">
          <img src="{{URL::asset('assets/img/attorney/'.$partners->partners_photo)}}" alt="attorney" class="img-responsive">
        </div>
        <!--<ul class="team-details-social">
          <li><a href="{{$partners->partners_facebook}}"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
          <li><a href="{{$partners->partners_twitter}}"><i aria-hidden="true" class="fa fa-twitter"></i></a></li>
          <li><a href="{{$partners->partners_linkedin}}"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
          <li><a href="{{$partners->partners_pinterest}}"><i aria-hidden="true" class="fa fa-pinterest"></i></a></li>
          <li><a href="{{$partners->partners_subscribe}}"><i aria-hidden="true" class="fa fa-rss"></i></a></li>
          <li><a href="{{$partners->partners_googleplus}}"><i aria-hidden="true" class="fa fa-google-plus"></i></a></li>
        </ul>
        <ul class="team-details-info">
          <li><i class="fa fa-map-marker"></i> {{$partners->partners_address}}</li>
          <li><i class="fa fa-phone"></i> {{$partners->partners_phone}}</li>
          <li><i class="fa fa-envelope-o"></i> {{$partners->partners_email}}</li>
          <li><i class="fa fa-fax"></i> {{$partners->partners_fax}}</li>
        </ul>-->
      </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
      <div class="team-details-content-holder">
        <h3>{{$partners->partners_name}}</h3>
        <h4 class="title-bar50">{{$partners->partners_position}}</h4>
        <p><span>{!!$partners->partners_overview!!}</span></p>
        <p>{!!$partners->partners_description!!}</p>                
        <!--<div class="skill1-area">
          <div class="progress">
            <div class="lead">{{$partners->partners_skill_1}}</div>
            <div data-wow-delay="1.2s" data-wow-duration="1.5s" style="width: 95%; visibility: visible; animation-duration: 1.5s; animation-delay: 1.2s; animation-name: fadeInLeft;" data-progress="{{$partners->partners_skill_1_point}}%" class="progress-bar wow fadeInLeft animated"> <span>{{$partners->partners_skill_1_point}}%</span></div>
          </div>
          <div class="progress">
            <div class="lead">{{$partners->partners_skill_2}}</div>
            <div data-wow-delay="1.2s" data-wow-duration="1.5s" style="width: 85%; visibility: visible; animation-duration: 1.5s; animation-delay: 1.2s; animation-name: fadeInLeft;" data-progress="{{$partners->partners_skill_2_point}}%" class="progress-bar wow fadeInLeft animated"><span>{{$partners->partners_skill_2_point}}%</span> </div>
          </div>
          <div class="progress">
            <div class="lead">{{$partners->partners_skill_3}}</div>
            <div data-wow-delay="1.2s" data-wow-duration="1.5s" style="width: 70%; visibility: visible; animation-duration: 1.5s; animation-delay: 1.2s; animation-name: fadeInLeft;" data-progress="{{$partners->partners_skill_3_point}}%" class="progress-bar wow fadeInLeft animated"><span>{{$partners->partners_skill_3_point}}%</span> </div>
          </div>
        </div>-->
      </div>
    </div>    
  </div>
</div> 
</div>
  <!-- Team details Inner Area End Here -->



@endsection