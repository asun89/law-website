@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
       <div class="banner-area" style="background-image:url({{URL::asset('assets/img/practice_area.jpg')}}) !important">
         <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="main-bennar">
                  <h2>{{trans('messages.practice_area')}}</h2>
                  <div class="breadcumb">
                    <ul>
                      <li> <a href="{{URL::to('home')}}">{{trans('messages.home')}}</a> </li>
                      <li>{{trans('messages.practice_area')}}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
       </div>
       <!--  Banner Area End here -->
       <!-- Practice Area Start Here -->
       <div class="practice-area practice-area-page">
          <div class="container">
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="practice-area-heading-section">
                    <h2>{{trans('messages.practice_area_title')}}</h2>
                    <!--<p>{{trans('messages.practice_area_description')}}</p>-->
                  </div>
                </div>
              </div>              
          </div>
        <div class="practice-service-section-area">
          <div class="container">
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                  @foreach ($practice_area as $pr)
                  <div class="single-practice-service-content-area practice_area_list">
                    <h3>{{$pr->practice_area_name}}</h3>
                  </div>
                  @endforeach
                </div>               
              </div>              
          </div>          
        </div>
       </div>
@endsection