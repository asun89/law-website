@extends('templates.layout')
@section('title', 'Page Title')

@section('content')<!--  Banner Area Start here -->
       <div class="banner-area" style="background-image:url({{URL::asset('assets/img/practice_area.jpg')}}) !important">
         <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="main-bennar">
                  <h2>{{$practice_area->practice_area_name}}</h2>
                  <div class="breadcumb">
                    <ul>
                      <li> <a href="{{URL::to('home')}}">Home</a> </li>
                      <li>{{$practice_area->practice_area_name}}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
       </div>
       <!--  Banner Area End here -->
    <!-- Blog Area Start -->
    <div class="total-blog-area">
      <div class="container">
        <div class="row">
          
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="blog-sidebar-area">
              <div class="single-sidebar">
                <h2>{{$practice_area->practice_area_name}}</h2>
                <div class="sidebar-category">
                  <ul>
                    @foreach ($practice_areas as $pr)
                    <li><a href="{{URL::to('practice_area/detail/'.$pr->id)}}">{{$pr->practice_area_name}}</a></li>
                    @endforeach
                  </ul>
                </div>
              </div>
              
            </div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="single-blog-post">
              <div class="blog-image">
                <a href="{{URL::to('practice_area/detail/'.$practice_area->id)}}"><img src="{{ URL::asset('img/blog/'.$practice_area->practice_area_image)}}" alt="family"></a>
              </div>
              <h2>{{$practice_area->practice_area_name}}</h2>
              <p class="text-bold">{!!nl2br(e($practice_area->practice_area_long_description))!!}</p>
              <h2>How Can We Help ?</h2>
              <p>{!!nl2br(e($practice_area->practice_area_solution))!!}</p>
            </div>
          
          </div>
        </div>
      </div>
    </div>
    <!-- Blog Area End -->
@endsection