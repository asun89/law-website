@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area" style="background-image:url({{URL::asset('assets/img/partners.jpg')}}) !important">
 <div class="container">
	<div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="main-bennar">
		  <h2>{{trans('messages.teams')}}</h2>
		  <div class="breadcumb">
			<ul>
			  <li> <a href="{{URL::to('home')}}">{{trans('messages.home')}}</a> </li>
			  <li>{{trans('messages.teams')}}</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<!--  Banner Area End here -->
<!-- Make An Appointment Area End here -->
<!-- Our Attorney Start Here -->
<div class="our-attorney-area">
<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>{{trans('messages.teams_title')}}</h2>
		 <!--<p>{{trans('messages.teams_caption')}}</p>-->
	   </div>
	</div>
  </div>
  <div class="our-attorney teams">
	
	@foreach($teams as $p)
	<!--<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="{{URL::to('teams/detail/'.$p->id)}}"><img src="{{URL::asset('assets/img/attorney/'.$p->teams_photo)}}" alt="{{$p->teams_name}}"></a>
		<div class="overlay">
		  <h2><a href="{{URL::to('teams/detail/'.$p->teams_id)}}">{{trans('messages.view_bio')}}</a></h2>
		  <div class="social-media">
			<ul>
			  <li><a href="{{$p->teams_facebook}}"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="{{$p->teams_twitter}}"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="{{$p->teams_linkedin}}"><i class="fa fa-linkedin"></i></a></li>
			  <li><a href="{{$p->teams_skype}}"><i class="fa fa-skype"></i></a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="{{URL::to('teams/detail/'.$p->teams_id)}}">{{$p->teams_name}}</a></h3>
		<p class="designation">{{$p->teams_position}}</p>
	  </div>
	</div>-->

	<div class="single-attorney col-md-3 margined_for_teams_section">
	  <div class="attorney-picture">
		<h2 class="team-name">{{$p->teams_name}}</h2>
	    <div class="borderline-team"></div>
	    <h3 class="team-position">{{$p->teams_position}}</h3>
	  </div>
	  <!--<div class="attorney-content">                
		<h3><a href="{{URL::to('teams/detail/'.$p->teams_id)}}">{{$p->teams_name}}</a></h3>
		<p class="designation">{{$p->teams_position}}</p>
	  </div>-->
	</div>


	<!--<div class="single-attorney">
	  <div class="attorney-picture">
		<a href="{{URL::to('teams/detail/'.$p->id)}}"><img src="{{URL::asset('assets/img/attorney/'.$p->teams_photo)}}" alt="{{$p->teams_name}}"></a>
		<div class="overlay">
		  <h2 class="team-name">{{$p->teams_name}}</h2>
		  <div class="borderline-team"></div>
		  <h3 class="team-position">{{$p->teams_position}}</h3>
		</div>
	  </div>
	  <div class="attorney-content">                
		<h3><a href="{{URL::to('teams/detail/'.$p->teams_id)}}">{{$p->teams_name}}</a></h3>
		<p class="designation">{{$p->teams_position}}</p>
	  </div>
	</div>-->
	@endforeach

  </div>
</div>
</div>
<!-- Our Attorney End Here -->
@endsection