<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Ariyanto Arnaldo Law Firm</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/img/favicon.png')}}">
		
		<!-- Google Fonts
		============================================ -->		
       
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css')}}">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
        
        <!-- nivo slider CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/custom-slider/css/nivo-slider.css')}}" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('assets/custom-slider/css/preview.css')}}" type="text/css" media="screen" />
		<!-- owl.carousel CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/owl.theme.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/owl.transitions.css')}}">
		<!-- jquery-ui CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery-ui.css')}}">
		<!-- meanmenu CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/meanmenu.min.css')}}">
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css')}}">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css')}}">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css')}}">        
        <!-- nivo slider CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/custom-slider/css/nivo-slider.css')}}" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('assets/custom-slider/css/preview.css')}}" type="text/css" media="screen" />
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css')}}">
    <!-- Responsive CSS
    ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/responsive.css')}}">

        <!-- CSS -->
        <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/alertify.min.css"/>
        <!-- Bootstrap theme -->
        <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/bootstrap.min.css"/>

        @foreach($css_files as $cf)
          <link rel="stylesheet" href="{{URL::asset($cf)}}" />
        @endforeach

		<!-- modernizr JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body class="{{$themes}}">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- Header Area Start Here -->
        <header>
        	<div class="header-area-top-area">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
        					<div class="header-top-left">
        						<p><i class="fa fa-map-marker"></i>{{trans('messages.working_address')}}</p>
        					</div>
        				</div>
        				<div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
        					<div class="header-top-right">
        						<nav>
                      <ul>
                        <li class="title-language-selection">Bahasa : </li>
                        <li><a href="{{url()->current()}}?lang=en"><img src="{{URL::asset('assets/img/flag/en.png')}}" width="20"></a></li>
                        <li><a href="{{url()->current()}}?lang=id"><img src="{{URL::asset('assets/img/flag/id.png')}}" width="20"></a></li>
                      </ul>
                    </nav>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
           <div class="main-header-area" style="background:#FFFFFF;">
                <div class="container">
                     <div class="row">                         
                          <div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
                              <div class="logo-area">
                                  <a href="{{ URL::to('home') }}"><img src="{{ URL::asset('assets/img/logo.png')}}" alt="logo"></a>
                              </div>
                          </div>  
                          <div class="col-lg-9 col-md-9 col-sm-12 col-sm-12">
                              <div class="main-menu-area">
                                  <nav>
                                      <ul>
                                          <!--<li class="current"><a href="{{ URL::to('home') }}"><i class="fa fa-home"></i></a></li>-->
                                          <li><a href="{{ URL::to('about') }}">{{trans('messages.about_us')}}</a></li>
                                          <li><a href="{{ URL::to('practice_area') }}">{{trans('messages.practice_area')}}</a></li>
                                          <li><a href="{{ URL::to('partners') }}">{{trans('messages.partners')}}</a></li>
                                          <li><a href="{{ URL::to('teams') }}">{{trans('messages.teams')}}</a></li>
                                          <li><a href="{{ URL::to('our_clients') }}">{{trans('messages.our_clients')}}</a></li>
                    										  <li><a href="{{ URL::to('events') }}">{{trans('messages.events')}}</a></li>
                    										  <li><a href="{{ URL::to('career') }}">{{trans('messages.career')}}</a></li>
                                          <li><a href="{{ URL::to('contact') }}">{{trans('messages.contact_us')}}</a></li>
                                      </ul>
                                  </nav>
                              </div>
                          </div>  
                          <!--<div class="col-lg-2 col-md-2 col-sm-2 hidden-sm col-sm-12">
                              <div class="search-area">
                                  <span><i class="fa fa-search"></i></span>  
                                  <input type="text" placeholder="Search Here....">
                              </div>
                          </div>--> 
                     </div>
                 </div> 
            </div>
            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area">
              <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="mobile-menu">
                    <nav id="dropdown">
                      <ul>
                        <li class="current"><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i></a></li>
          						  <li><a href="{{ URL::to('about') }}">About Us</a></li>
          						  <li><a href="{{ URL::to('practice_area') }}">Practice Areas</a></li>
          						  <li><a href="{{ URL::to('partners') }}">Partners</a></li>
          						  <li><a href="{{ URL::to('our_clients') }}">Our Clients</a></li>
          						  <li><a href="{{ URL::to('events') }}">Events</a></li>
          						  <li><a href="{{ URL::to('career') }}">Career</a></li>
          						  <li><a href="{{ URL::to('contact') }}">CONTACT US</a></li>
                      </ul>
                    </nav>
                  </div>          
                </div>
              </div>
              </div>
            </div>
            <!-- mobile-menu-area end -->
        </header>
        <!-- Header Area End Here --> 
		@yield('content')
		
		<!-- Partner Logo Area Start Here -->
      <!--<div class="client-logo-area">
        <div class="container">
          <div class="client-logo">
            <div class="single-logo">
              <a href="#"><img src="{{ URL::asset('assets/img/client/1.jpg')}}" alt=""></a>
            </div>
            <div class="single-logo">
              <a href="#"><img src="{{ URL::asset('assets/img/client/2.jpg')}}" alt=""></a>
            </div>
            <div class="single-logo">
              <a href="#"><img src="{{ URL::asset('assets/img/client/3.jpg')}}" alt=""></a>
            </div>
            <div class="single-logo">
              <a href="#"><img src="{{ URL::asset('assets/img/client/4.jpg')}}" alt=""></a>
            </div>
            <div class="single-logo">
              <a href="#"><img src="{{ URL::asset('assets/img/client/5.jpg')}}" alt=""></a>
            </div>
            <div class="single-logo">
              <a href="#"><img src="{{ URL::asset('assets/img/client/6.jpg')}}" alt=""></a>
            </div>
            <div class="single-logo">
              <a href="#"><img src="{{ URL::asset('assets/img/client/7.jpg')}}" alt=""></a>
            </div>
          </div>
        </div>
      </div>-->
      <!-- Partner Logo Area End Here -->
      <!-- Get Free Consultation Start Here -->
      <div class="get-free-consultation-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
              <div class="consultation-text">
                <h3>{{trans('messages.consultation_message')}}</h3>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="contact-buttom">
                <a href="{{URL::to('contact')}}">{{trans('messages.consultation_button_text')}}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Get Free Consultation End Here -->
      <!-- Footer Area Start Here -->
      <footer>
        <div class="footer-top-area">
          <div class="container">
            <div class="row">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="footer footer-one">
                  <h2>{{trans('messages.about_us')}}</h2>
                  <p>{!!nl2br(e(trans('messages.home_message_1')))!!}</p>             
                  <div class="social-media">
                    <ul>
                      <li><a href="{{trans('messages.facebook_link')}}"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="{{trans('messages.twitter_link')}}"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="{{trans('messages.linkedin_link')}}"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="{{trans('messages.facebook_link')}}"><i class="fa fa-skype"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="footer footer-two">
                  <h2>Explore Our Work</h2>
                  <ul>
                    <li><a href="#">Criminal law</a></li>
                    <li><a href="#">Accident law</a></li>
                    <li><a href="#">Devorce law</a></li>
                    <li><a href="#">Industrial law</a></li>
                    <li><a href="#">Business law</a></li>
                    <li><a href="#">Familly law</a></li>
                  </ul>                  
                </div>
              </div>-->
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="footer footer-three">
                  <h2>{{trans('messages.keep_in_touch')}}</h2>
                  <ul>
                    <li><i class="fa fa-clock-o"></i> {{trans('messages.working_day')}}</li>
                    <li><i class="fa fa-map-marker"></i> {{trans('messages.working_address')}}</li>
                    <li><i class="fa fa-phone"></i> {{trans('messages.working_phone')}}</li>
                    <li><i class="fa fa-envelope-o"></i> {{trans('messages.working_email')}}</li>
                    <li><i class="fa fa-fax"></i> {{trans('messages.working_fax')}}</li>
                  </ul>                  
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                 <div class="footer footer-two">
                  <h2>{{trans('messages.sitemap')}}</h2>
                  <ul>
                    <li><a href="{{URL::to('about')}}">{{trans('messages.about_us')}}</a></li>
                    <li><a href="{{URL::to('practice_area')}}">{{trans('messages.practice_area')}}</a></li>
                    <li><a href="{{URL::to('partners')}}">{{trans('messages.partners')}}</a></li>
                    <li><a href="{{URL::to('our_clients')}}">{{trans('messages.our_clients')}}</a></li>
                    <li><a href="{{URL::to('events')}}">{{trans('messages.events')}}</a></li>
                    <li><a href="{{URL::to('career')}}">{{trans('messages.career')}}</a></li>
                    <li><a href="{{URL::to('contact')}}">{{trans('messages.contact_us')}}</a></li>
                  </ul>                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-buttom-area">
          <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="footer-buttom">
                    <div class="scrollup"><a href="#"><i class="fa fa-chevron-up"></i></a></div>
                   <p>Ariyanto & Arnaldo Law Firm © All Rights Reserved</p>
                 </div>
               </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- Footer Area End Here -->
		<!-- jquery
		============================================ -->		
        <script src="{{ URL::asset('assets/js/vendor/jquery-1.11.3.min.js')}}"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
		<!-- wow JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/wow.min.js')}}"></script>
		<!-- price-slider JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/jquery-price-slider.js')}}"></script>		
		<!-- meanmenu JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/jquery.meanmenu.js')}}"></script>
		<!-- owl.carousel JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/owl.carousel.min.js')}}"></script>
		<!-- scrollUp JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/jquery.scrollUp.min.js')}}"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/plugins.js')}}"></script>
        <!-- Nivo slider js
        ============================================ -->        
        <script src="{{ URL::asset('assets/custom-slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
        <script src="{{ URL::asset('assets/custom-slider/home.js')}}" type="text/javascript"></script>
        <script src="//cdn.jsdelivr.net/alertifyjs/1.9.0/alertify.min.js"></script>
		<!-- main JS
		============================================ -->		

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
        
        <script src="{{ URL::asset('assets/js/main.js')}}"></script>

        @foreach($js_files as $jf)
          <script src="{{URL::asset($jf)}}"></script>
        @endforeach

        <script>
        {!!$custom_js!!}
        </script>

    </body>
</html>
